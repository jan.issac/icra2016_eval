#!/usr/bin/python2

"""
Created by C. Garcia Cifuentes on 2015-09-15

Generate fake data imitatings Jan's format.


"""


import numpy as np
import time

import warnings
warnings.filterwarnings("ignore")

from transformations import unit_vector, quaternion_about_axis


def _one_line(f1, f2, f3):
    t1 = 10.0*np.random.randn(3)
    angle = 2.0*np.pi*np.random.rand()
    axis = unit_vector(np.random.rand(3) - 0.5)
    q1 = quaternion_about_axis(angle, axis)
    
    t_err = np.array([2.0 if (np.random.rand() < 0.4) else 5.0] + 
                     np.random.randn(2).tolist())            
    angle_err = np.pi/4

    t2 = t1 + t_err
    q2 = quaternion_about_axis(angle + angle_err, axis)


    t3 = t1 - 5.*t_err
    q3 = quaternion_about_axis(angle + 2*angle_err, axis)

    ts = time.time()

    print >>f1, ts, ' '.join(['{:7.3f}'.format(n) for n in 
                              t1.tolist() + q1.tolist()])
    print >>f2, ts, ' '.join(['{:7.3f}'.format(n) for n in 
                              t2.tolist() + q2.tolist()])
    print >>f3, ts, ' '.join(['{:7.3f}'.format(n) for n in 
                              t3.tolist() + q3.tolist()])


def main():

    fname1 = 'fake_data/run_0/fake/gt.txt'
    fname2 = 'fake_data/run_0/fake/sth1.txt'
    fname3 = 'fake_data/run_0/fake/sth2.txt'
    header = '#timestamp tx ty tz qw qx qy qz\n'

    with open(fname1, 'w') as f1:
        with open(fname2, 'w') as f2:
            with open(fname3, 'w') as f3:
                f1.write(header)
                f2.write(header)
                f3.write(header)
                for i in xrange(2500):
                    _one_line(f1, f2, f3)



if __name__ == '__main__':
    main()
