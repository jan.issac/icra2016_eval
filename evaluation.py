#!/usr/bin/python2

"""
Created by C. Garcia Cifuentes on 2015-09-15

Error measures

"""

import numpy as np

from matplotlib import rcParams
rcParams['pdf.fonttype'] = 42
#rcParams['ps.useafm'] = True
#rcParams['pdf.use14corefonts'] = True
#rcParams['text.usetex'] = False 

import matplotlib.pyplot as plt

import itertools


_legend_kwargs = {'loc': 'best', 'frameon': False, 'prop': {'size': 12}}



def simple_stats(err, qs=[10., 50., 90.]):
    """Compute mean, standard deviation and some representative percentiles."""
    mean = np.mean(err)
    std = np.std(err)
    ps = np.percentile(err, qs)
    return mean, std, ps


def histograms(errors, labels, colors, nbins=200, ax=None, mode='lin-lin'):
    """
    Joint plot of error histograms.

    mode 'lin-lin': uniform bins, linear xscale.
         'lin-log': uniform bins, log xscale.
         'log-log': log bins, log xscale.

    """
    
    assert(mode in ['lin-lin', 'lin-log', 'log-log'])

    if ax is None:
        _, ax = plt.subplots(1, 1)

    # Determine bin edges
    low, high = errors.min(), errors.max()
    if mode.startswith('log'):
        bins = np.exp(np.linspace(np.log(low), np.log(high), nbins))
    else:
        bins = np.linspace(low, high, nbins)
    ## [debug]
    #plt.plot(bins); plot.pause(0.1)
    #import ipdb; ipdb.set_trace()
    
    # Plot
    lines = []
    for err, lab, col in zip(errors, labels, colors):
        h, _, _ = ax.hist(err, bins, normed=True, histtype='stepfilled', 
                          color=col, alpha=0.2, label=lab)
    # (I don't know how to do these two in one single step,
    # so I repeat the for loop)
    for err, lab, col in zip(errors, labels, colors):
        h, _, _ = ax.hist(err, bins, normed=True, histtype='step', 
                          color=col, label=lab)
        # check that the resulting histogram is properly normalized
        assert(np.abs(np.sum(h*(bins[1:]-bins[:-1])) - 1.0) < 1e-6)
        # prepare lines for legend 
        lines.append(plt.Line2D([0], [0], color=col, lw=2))
    
    # Fix axes
    ax.set_xlim([bins[0], bins[-1]])

    # Legend
    ax.legend(lines, labels, **_legend_kwargs)

    # Apply log scale
    if mode.endswith('log'):
        ax.set_xscale('log')

    

def temporal_plot(errors, labels, colors, axs=None, fps=30.0, 
                  stop1=None, stop2=50, legend=None): 
    """
    Joint plot of errors over time.
    Two plots with different time windows.

    Arguments: 
    - errors: iterable of error-over-time arrays, e.g. from different
        methods.  
    - labels: iterable of methods names, for legend.  
    - colors: iterable of colors, for plot and legend.
    - fps: frames per second, i.e. inverse of time step, for appropriate
        scaling of time axis.
    - stop1, stop2: plot from timestep 0 up to stopX.
    - legend: {1, 2, None} put legend on first or second plot, or don't.
    
    """

    if axs is None:
        _, axs = plt.subplots(2, 1)

    # number of timesteps
    n = errors.shape[1]
    time = np.arange(n)/fps
    stop = [n if s is None else min(n, int(s)) for s in [stop1, stop2]]

    # Two plots with different time scales
    for ax, s in zip(axs, stop):

        # Plot error over time for each method
        lines = itertools.cycle(['-', '--', ':'])
        for err, lab, col in zip(errors, labels, colors):
            ls = next(lines)
            ax.plot(time[:s], err[:s], color=col, label=lab, lw=2, linestyle=ls)
    
        # Set axes limits
        # - time to min and max
        ax.set_xlim(time[0], time[s-1])
        # - error axis is extended a little
        yticks = ax.get_yticks()
        h = yticks[1] - yticks[0]
        ylim = ax.get_ylim()
        ax.set_ylim([ylim[0] - h, ylim[1] + h])
    
        # Set powers at which axis formatter switches to scientific notation,
        # i.e. for numbers below 1e0 and above 1e1
        # [doc: http://matplotlib.org/api/ticker_api.html#
        #              matplotlib.ticker.ScalarFormatter.set_powerlimits]
        # [see also: http://stackoverflow.com/questions/14775040]
        ax.get_yaxis().get_major_formatter().set_powerlimits((0, 1))
                         
    # Legend
    if legend is not None:
        l = int(legend) - 1 
        axs[l].legend(**_legend_kwargs)


if __name__ == '__main__':
    main()
