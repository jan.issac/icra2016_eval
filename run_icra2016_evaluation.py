#!/usr/bin/python2

"""
Created by C. Garcia Cifuentes on 2015-09-15

Evaluation of tracking performance.


Commands for paper:
$ python run_icra2016_evaluation.py occ calibration
$ python run_icra2016_evaluation.py occ fully
$ python run_icra2016_evaluation.py 0 30

Available commands:
$ python run_icra2016_evaluation.py occ [calibration|canteen|impact]
$ python run_icra2016_evaluation.py occ fully 
$ python run_icra2016_evaluation.py <start runno> <end runno>

To run all, simply:
$ python run_icra2016_evaluation.py all

"""

import numpy as np

import sys
import os

import warnings

from transformation_interface import Pose3d, compare_poses, NanTrackingPose
from evaluation import simple_stats, histograms, temporal_plot, plt



# Change this to the paper repository or whatever output folder 
_odir = os.path.expanduser(
    '~/writing/3d_object_tracking_using_a_gaussian_filter')

# Change this to point at the results
_idir = './remote_results'

# Make this true if you want the program to pause, give you a chance to modify 
# figures, then press 'c' to continue and save them.
# Otherwise they are saved automatically.
_pause = False


# Some figure config
_scheme = {'blueish': np.array((103., 135., 176.))/255,
           'greenish': np.array((177., 177., 123.))/255,
           'reddish': np.array((205., 102., 7.))/255,
           'orangeish': np.array((246., 160., 61.))/255}

_colors = {'RGF': _scheme['blueish'], 
           'PF': _scheme['reddish'],
           'robust GF': _scheme['blueish'], 
           'non robust GF': _scheme['greenish'],
           'sth1': 'blue', 
           'sth2': 'red'}

_figsize = (7, 6)




class _Dset(object):
    """
    A class to read 3D tracking results.
    File paths and file format as Jan generated them.

    """

    _pattern = '{idir}/run_{runno}/{seq}/{method}.txt'
    _gt = '{idir}/run_{runno}/{seq}/gt.txt'
    _method_dict = {'RGF': 'gf', 'PF': 'pf'}

    def __init__(self, idir, runs, sequences=None, methods=None):
        self._idir = idir
        self._runs = runs
        if sequences is None:
            self._init_sequences()
        else:
            self._sequences = sequences
        if methods is None:
            self._methods = ['RGF', 'PF']
        else:
            self._methods = methods
        
    @property
    def methods(self):
        return self._methods

    @property
    def runs(self):
        return self._runs

    @property
    def sequences(self):
        return self._sequences

    @property
    def idir(self):
        return self._idir

    def _init_sequences(self):
        # Infer sequences names from folder content
        path = '{}/run_{}'.format(self.idir, self._runs[0])
        self._sequences = [f for f in os.listdir(path) 
                           if os.path.isdir(path + '/' + f)]

    def get_fnames(self):
        """
        Get all relevant fnames in the dataset, for all runs and all sequences,
        i.e. ground-truth file and a results file per method.

        """
        args = {'idir': self._idir}
        all_fnames = []
        for runno in self.runs:
            for seq in self.sequences:
                args['runno'], args['seq'] = runno, seq
                # add fname of gt file
                fnames = [self._gt.format(**args)]
                # add fname of results of each method
                for method in self._methods:
                    args['method'] = self._method_dict[method]
                    fnames.append(self._pattern.format(**args))
                all_fnames.append(fnames)
        return all_fnames


    def read_file(self, fname):
        """Read 3D poses from text file, one per line."""

        # assuming a small file, we can read it all at once
        with open(fname, 'r') as fp:
            lines = fp.readlines()
        
        times = []
        poses = []
        for l in lines:
            if l[0] == '#':
                continue
            parts = l.split()  # split at whitespaces and strip
            try:
                t, p = float(parts[0]), Pose3d(*parts[1:])
            except TypeError:
                continue
            times.append(t)
            poses.append(p)

        # return as dict instead of separate lists, for easier correspondence 
        # across files
        return {t: p for t, p in zip(times, poses)}



# Variants of the basic dataset (slightly different file path pattern,
# but methods for parsing files can be reused).

class _OccDset(_Dset):
    
    _pattern = '{idir}/{method}/run_{runno}/{seq}/gf.txt'
    _gt = '{idir}/heavy_tailed/run_{runno}/{seq}/gt.txt'
    _method_dict = {'robust GF': 'heavy_tailed',
                    'non robust GF': 'zero_tail'}

    def __init__(self, idir, runs, sequences=None, methods=None):
        if methods is None:
            methods = ['robust GF', 'non robust GF']
        _Dset.__init__(self, idir, runs, sequences, methods)



class _OccPFDset(_Dset):
    
    _pattern = '{idir}/{method}.txt'
    _gt = '{idir}/gt.txt'
    _method_dict = {'RGF': 'gf', 'PF': 'pf'}

    def __init__(self, idir, methods=None):
        if methods is None:
            methods = ['RGF', 'PF']
        _Dset.__init__(self, idir, [0], [''], methods)



def _process_dset(dset):
    """
    Get all errors from dataset.

    Return arrays of shape (num_methods, total_num_timesteps),
    where total_num_timesteps = \sum_i num_timesteps_i,
    and i runs over sequences.

    This means that the "weight" of each sequence in the global preformance 
    is "proportional" to its length.

    """

    all_fnames = dset.get_fnames()
    trans_err = []
    ang_err = []
    blacklist = []

    for fnames in all_fnames:

        run_seq_name = os.path.dirname(fnames[0])
        run_seq_name = '/'.join(run_seq_name.split('/')[-2:])
        print(run_seq_name)
    
        # Read files for a single sequence
        gt = dset.read_file(fnames[0])
        others = [dset.read_file(fn) for fn in fnames[1:]]

        # Remove non corresponding entries
        times = set(gt.keys())
        init_len = len(times)
        for o in others:
            times.intersection_update(set(o.keys()))
        times = sorted(list(times))
        if len(times) < init_len:
            warnings.warn("not all timestamps could be matched")
        gt_poses = [gt[t] for t in times]
        other_poses = [[o[t] for t in times] for o in others]
            
        # Get arrays of shape (num_timesteps, num_methods)
        try:
            te, ae = compare_poses(gt_poses, other_poses)
        except NanTrackingPose as e:
            # Add run&sequence to blacklist 
            blacklist.append(run_seq_name)
        else:
            # Append to list 
            trans_err.append(te)
            ang_err.append(ae)

    # Concatenate error of all sequences along timesteps dimension.
    # Then transpose to get arrays of shape (num_methods, total_num_timesteps)
    trans_err = np.concatenate(trans_err, axis=0).T
    ang_err = np.concatenate(ang_err, axis=0).T

    return trans_err, ang_err, blacklist



# Helper functions to initialize _Dset objects and set figures output 
# directory, tailored to this ICRA paper.
#
# /!\ The path to input is harcoded to be ./remote_results. 
# Change this at the beginning of this file, 
# or within these helper functions. 
#
# /!\ The destination folders for the figures and stats are harcoded to be 
# somewhere in the paper repository.Change this at the beginning of this file, 
# or within these helper functions.  


def _get_fake_dset():
    dset= _Dset(idir='./fake_data', runs=[0], sequences=['fake'],
                methods=['sth1', 'sth2'])
    odir = dset.idir
    return dset, odir
    

def _get_testrun_dset():
    dset = _Dset(idir='./icra_results_testrun', runs=[0], 
                 sequences=['calibration_object_fast2', 
                            'impact_battery_occlusion-slow'])
    odir = dset.idir
    return dset, odir
    

def _get_full_dset(start, end):

    dset = _Dset(idir=(_idir + '/icra_results_30_runs'), 
                 runs=range(start, end))
    # (gets sequences names from folder content; check visually)
    print dset.sequences
    print len(dset.sequences)

    odir = '{}/results/runs_{}_{}'.format(_odir, dset.runs[0], dset.runs[-1])
    assert(os.path.isdir(odir))

    return dset, odir


def _get_full_dset2():

    dset = _Dset(idir=(_idir + '/icra_results'), runs=[0])
    # (gets sequences names from folder content; check visually)
    print dset.sequences
    print len(dset.sequences)

    odir = '{}/results/runs_{}_{}_bis'.format(_odir, dset.runs[0], dset.runs[-1])
    assert(os.path.isdir(odir))

    return dset, odir


def _get_occ_dset(seq):

    expand = {'calibration': 'calibration_object_occlusion-3',
              'canteen': 'canteen_occlusion',
              'impact': 'impact_battery_occlusion-slow'}
    assert(seq in expand.keys())

    dset = _OccDset(idir=(_idir + '/icra_result_occlusion'), runs=[0],
                    sequences=[expand[seq]])

    odir = '{}/results/occlusion/{}'.format(_odir, seq)
    assert(os.path.isdir(odir))

    return dset, odir

    
def _get_occpf_dset():

    dset = _OccPFDset(idir='./remote_results/icra_results_fully_occluded')

    odir = '{}/results/occlusion/fully'.format(_odir)
    assert(os.path.isdir(odir))

    return dset, odir


def _pre_main(dset, odir):

    # get all errors in dataset
    trans_err, ang_err, blacklist = _process_dset(dset)

    # print blacklist
    with open('{}/blacklist.txt'.format(odir), 'w') as fp:
        for bl in blacklist:
            print >>fp, bl

    # get simple stats
    stats_names = ['mean', 'std', 'p10', 'p50', 'p90']

    def _join(vec):
        return '\t'.join(['{:8.5f}'.format(v) for v in vec])

    def _print(f, err_vec, err_name):
        stats = [simple_stats(err) for err in err_vec]
        stats = zip(*[(s1, s2, s3, s4, s5) for s1, s2, (s3, s4, s5) in stats])
        for s, sn in zip(stats, stats_names):
            print >>f, '{} err {}:\t{}'.format(err_name, sn, _join(s))

    with open('{}/stats.txt'.format(odir), 'w') as fp:
        print >>fp,'#              \t{}'.format(
            '\t'.join(['{:>8}'.format(m) for m in dset.methods]))
        _print(fp, trans_err, 'trans')
        _print(fp, ang_err, 'ang')

    return trans_err, ang_err



def main(argv):

    # Set dataset parameters: path, run numbers, sequence names, etc.
    
    # [these were for debug]
    #dset, odir = _get_fake_dset()
    #dset, odir = _get_testrun_dset()

    if len(argv) < 2:
        dset, odir = _get_full_dset2()
        # [debug]
        #import ipdb; ipdb.set_trace()
    else:
        dset, odir = _get_full_dset(int(argv[0]), int(argv[1]))
        
    # Process dataset, get all errors, print some stats to file
    trans_err, ang_err = _pre_main(dset, odir)
    colors = [_colors[k] for k in dset.methods]

    # Plot error histograms
    figs = []
    hist_modes = ['lin-lin', 'lin-log', 'log-log'] 
    for mode in hist_modes:
        fig, axs = plt.subplots(2, 1, figsize=_figsize)
        figs.append(fig)

        histograms(trans_err, labels=dset.methods, colors=colors, 
                   nbins=200, ax=axs[0], mode=mode)
        axs[0].set_xlabel('translational error (m)')

        histograms(ang_err, labels=dset.methods, colors=colors, 
                   nbins=200, ax=axs[1], mode=mode)
        axs[1].set_xlabel('angular error (rad)')
        
        fig.tight_layout()

    plt.pause(0.1)
    if _pause:
        # Pause, modify figs if needed, then save
        import ipdb; ipdb.set_trace()
    for fig, mode in zip(figs, hist_modes):
        fig.savefig('{}/hist-{}.pdf'.format(odir, mode))
    

def main_occ(key):
    
    # Set dataset parameters: path, run numbers, sequence names, etc.
    # Set also some hacky parameters for good-looking plots.
    if key == 'fully':
        dset, odir = _get_occpf_dset()
        stop1 = None
        stop2 = 40 # timestep before occlusion happens
    else:
        dset, odir = _get_occ_dset(key)
        stop1 = 400
        stop2 = 40 # timestep before occlusion happens

    # Process dataset, get all errors, print some stats to file.
    trans_err, ang_err = _pre_main(dset, odir)

    # Plot errors over time
    colors = [_colors[k] for k in dset.methods]
    fig, axs = plt.subplots(2, 2, figsize=_figsize)

    temporal_plot(trans_err, labels=dset.methods, colors=colors, axs=axs[0],
                  stop1=stop1, stop2=stop2, legend=1)
    axs[0,0].set_ylabel('translational error (m)')
    
    temporal_plot(ang_err, labels=dset.methods, colors=colors, axs=axs[1],
                  stop1=stop1, stop2=stop2)
    axs[1,0].set_ylabel('angular error (rad)')

    for ax in axs.flat:
        ax.set_xlabel('time (s)')

    
    fig.tight_layout()
    plt.pause(0.1)
    if _pause:
        # Pause, modify figs if needed, then save
        import ipdb; ipdb.set_trace()
    fig.savefig('{}/temporal.pdf'.format(odir))
    


if __name__ == '__main__':

    if len(sys.argv) == 2 and sys.argv[1] == 'all':
        for key in ['calibration', 'canteen', 'impact', 'fully']:
            main_occ(key)
        main(['0', '30'])

    elif len(sys.argv) > 2 and sys.argv[1] == 'occ':
        main_occ(sys.argv[2])

    else:
        main(sys.argv[1:])


