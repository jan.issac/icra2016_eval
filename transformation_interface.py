#!/usr/bin/python2

"""
Created by C. Garcia Cifuentes on 2015-09-15

Classes to compute relative pose and translational and angular difference.

"""

import numpy as np

import warnings
warnings.filterwarnings("ignore")

from transformations import unit_vector
from transformations import quaternion_matrix, quaternion_from_matrix



class Quaternion(object):

    def __init__(self, w, x, y, z):
        self._q = unit_vector(np.array([w, x, y, z], dtype=np.float32))
        self._r = None  # lazy init

    @property
    def q(self):
        """The quaternion [w, x, y, z]."""
        return self._q

    @property
    def R0(self):
        """The corresponding homogeneous rotation matrix."""
        if self._r is None:
            self._r = quaternion_matrix(self._q)
        return self._r

    @property
    def R(self):
        """The corresponding 3x3 rotation matrix."""
        return self.R0[:3,:3]


class Pose3d(object):

    def __init__(self, tx, ty, tz, qw, qx, qy, qz):
        self._t = np.array([tx, ty, tz], dtype=np.float32)
        self._qobj = Quaternion(qw, qx, qy, qz)
        self._m = None  # lazy init

    @property
    def isnan(self):
        return any([np.any(np.isnan(v)) for v in [self.q, self.M]])

    @property
    def t(self):
        """The translation [tx, ty, tz]."""
        return self._t

    @property
    def q(self):
        """The quaternion [qw, qx, qy, qz]."""
        return self._qobj.q

    @property
    def R(self):
        """The 3x3 rotation matrix."""
        return self._qobj.R

    @property
    def M(self):
        """The corresponding homogeneous transformation matrix."""
        if self._m is None:
            self._m = self._qobj.R0.copy()
            self._m[:3, 3] = self._t[:]
        return self._m

    def distance(self):
        return np.linalg.norm(self.t)

    def angle(self):
        # angle directly from unit quaternion
        a = 2.0*np.arctan2(np.linalg.norm(self.q[1:]), self.q[0])
        # angle from rotation matrix
        cos_a = 0.5*(np.trace(self.R) - 1.)
        other_a = np.arccos(np.clip(cos_a, -1., 1.))
        # [debug] both should be the same
        assert(np.allclose(a, other_a, atol=1e-9))
        return a

    @classmethod
    def relative(cls, one, other):
        """Relative 3D transformation from self to other."""
        rel_matrix = np.linalg.solve(one.M, other.M)
        return cls.from_matrix(rel_matrix)
    
    @classmethod
    def from_matrix(cls, homog_matrix):
        """
        Build Pose3d object from a precise homogeneous transformation 
        matrix.

        """
        # check that it's homogeneous as expected
        assert(np.allclose(homog_matrix[3], [0., 0., 0., 1.]))
        # extract translation
        t = homog_matrix[:3, 3]
        # use library 'transformations.py' to get the quaternion
        m = homog_matrix.copy()
        m[:3, 3] = 0.
        q = quaternion_from_matrix(m, isprecise=False)
        # construct object from translation and quaternion
        args = t.tolist() + q.tolist()
        obj = cls(*args)
        # [debug] check that we get the same matrix back
        assert(np.allclose(obj.R, homog_matrix[:3, :3], atol=1e-6))
        assert(np.allclose(obj.t, homog_matrix[:3, 3], atol=1e-6))

        return obj


class NanTrackingPose(ValueError):
    pass


def compare_poses(ref_poses, other_poses):
    """Return arrays of relative distance and angle."""
    trans_err = []
    ang_err = []
    for ps in zip(ref_poses, *other_poses):
        pose1 = ps[0]
        if pose1.isnan:
            # due to GT adquisition; this can be ignored
            continue
        te = []
        ae = []
        for pose2 in ps[1:]:
            if pose2.isnan:
                # this is not normal
                raise NanTrackingPose()
            rel_pose = Pose3d.relative(pose1, pose2)
            te.append(rel_pose.distance())
            ae.append(rel_pose.angle())
        trans_err.append(te)
        ang_err.append(ae)
    return np.array(trans_err), np.array(ang_err)


